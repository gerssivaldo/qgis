import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">

        {/* Site top bar with home button and sections */}
        <div className="top-bar">
          <div className="top-bar-left">
            <a className="nav-home" href="/">
              <img src={logo} className="App-logo" alt="logo" />
            </a>
          </div>
          <div className="top-bar-right"></div>
        </div>

      </header>
    </div>
  );
}

export default App;
